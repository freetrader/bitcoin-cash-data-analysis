#!/usr/bin/env python3
#
# Copyright (c) 2021 freetrader
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
Reads verbose block JSON output from `getblock` RPC call
and calls `bitcoin-cli getrawtransacton` for each transaction
in the block, then grabs the transaction version and tallies
up how many transactions for each version.

Output is of the form

     blockheight,version,count

with one line per transaction version found in the block,
and the `count` being how many transactions in the block
had that version.

Caveat: this is some ugly, slow, unsupported code.
"""

import json
import sys
import subprocess

from subprocess import PIPE


def get_json_for_tx(tx_hash):
    '''
    Call bitcoin-cli to get the verbose JSON for a transaction
    identified by the tx_hash. bitcoin-cli must be in the PATH.
    '''
    session = subprocess.Popen(['bitcoin-cli', "getrawtransaction",
                                str(tx_hash), "true"], stdout=PIPE, stderr=PIPE)
    stdout, stderr = session.communicate()
    if stderr:
        raise Exception("Error "+str(stderr))
    return json.loads(stdout)

json_input = sys.stdin.read()
blk_deser = json.loads(json_input)

# dict to keep txversion:count
tx_versions = {}

# count the tx versions over txs in the block
for tx in blk_deser['tx']:
    tx_json = get_json_for_tx(tx)
    tx_ver = int(tx_json['version'])
    if tx_ver not in tx_versions:
        tx_versions[tx_ver] = 1
    else:
        tx_versions[tx_ver] = tx_versions[tx_ver] + 1

# output the results sorted by ascending tx version
for v in sorted(tx_versions.keys()):
    print("{},{},{}".format(blk_deser['height'], v, tx_versions[v]))
