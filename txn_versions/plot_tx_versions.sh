#!/bin/bash
# Create $1.dat and $1.gnuplot files

datafile="$1"

VERSIONS=$(awk -F, '{print $2}' "$1" | sort -u)

rm -f all.dat
touch all.dat

echo "set title 'Transaction versions in $1' noenhanced" > all.gnuplot
echo "set x2label 'block height'" >> all.gnuplot
echo "set ylabel 'count'" >> all.gnuplot
echo "set grid" >> all.gnuplot
echo "set key below center horizontal noreverse enhanced autotitle" >> all.gnuplot
echo "set tics out nomirror" >> all.gnuplot
echo 'plot \' >> all.gnuplot

# range counter
index=0

for v in $VERSIONS
do
  grep ",${v}," "$1" | sed 's/,/ /g' | sort -n -k 2 > ${v}.dat
  cat ${v}.dat >> all.dat
  echo >> all.dat
  echo >> all.dat
  echo "  'all.dat' index ${index} using 1:3 with points pointtype $((${index} + 1)) title \"v${v}\", \\" >> all.gnuplot
  index=$(($index + 1))
  rm -f "./${v}.dat"
done

echo >> all.gnuplot
