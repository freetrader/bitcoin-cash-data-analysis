# Data about transaction `version` fields used

## Data files

CSV files in this repository are currently being filled with data.
Their filename describes the intended range, but the data is not
being filled into them sequentially, but in walks over the range
with iteratively decreasing step sizes.

Each line in the `.csv` files is of the format

    blockheight, tx_version, count_of_txs_with_that_version

(without the spaces)

This should be suitable for easy import using tools that can
process comma-separated value (CSV) files.

The files are named:

- `tx_versions_<start_height>_<end_height>_10000_1.csv` :
  data for blocks from `<start_height> to <end_height>,
  starting with a step size of 10,000 and decreasing down to
  1 (i.e. ultimately covering every value in the range).

Some of these files are complete, others are still in progress.
Data in those file will be added automaticaly at some more or less
regular intervals.

The data from 0 - 399,999 was deemed less interesting initially,
so it was started later. However, by chance the first non-{v1,v2}
transaction version was detected in that block range.

## Scripts

* `tx_version_stats_from_block_verbose_json.py` :

  Retrieves the version data for a single block from an indexed
  full node (transaction index enabled via `txindex=1`)

* plot_tx_versions.sh

  Takes a CSV filename, and creates a data file `.dat` and a
  gnuplot file (.gnuplot), for plotting the versions found.
