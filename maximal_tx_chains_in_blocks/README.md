This folder contains data on the number of transactions
in the longest chain found in a given BCH block.

The script used to extract this maximal tx chain length
is `get_longest_chain_in_verbose_block_json.py`.
It needs Python3 and the NetworkX module to run.

Data files here are split up into epochs:

From beginning of BCH until May 2020, when the unconfirmed
tx chain limits were 25 transactions:

- 478558-635257_aug2017_to_may2020.csv

After May 2020, the limits were raised from 25 to 50:

- 635258-661647_may2020_to_axion_nov2020.csv
- 661648-688092_axion_to_tachyon_may2021.csv

In May 2021, the limits were lifted completely:
(only limited by block size)

- 688093-688381_tachyon_may2021_and_beyond.csv
