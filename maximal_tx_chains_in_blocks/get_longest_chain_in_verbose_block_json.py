#!/usr/bin/env python3
# Copyright (c) 2021 freetrader
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
Read verbose BCH block JSON output (from getblock RPC)
from standard input and print out the length of the
longest transaction chain found in that block.
"""

import json
import sys
import networkx as nx

json_input = sys.stdin.read()
blk_deser = json.loads(json_input)
G = nx.DiGraph()

# first pass to collect all the tx hashes in the block
tx_hashes = []
tx_vins = {}
for tx_dict in blk_deser['tx']:
    tx_hash = tx_dict['hash']    
    tx_hashes.append(tx_hash)
    tx_vins[tx_hash] = tx_dict['vin']
    # all tx hashes are added as graph nodes
    G.add_node(tx_hash)

# next we need to inspect the vin's of each tx to see if
# we need to add edge to any other txs in the block
for tx_hash in tx_hashes:
    for vin in tx_vins[tx_hash]:
        if 'coinbase' in vin:
            pass
        else:
            # not a coinbase tx, so let's look at the vin txids
            vin_txid = vin['txid']
            if vin_txid in tx_hashes:
                G.add_edge(tx_hash, vin_txid)

longest_path = nx.dag_longest_path(G)
print(len(longest_path))
