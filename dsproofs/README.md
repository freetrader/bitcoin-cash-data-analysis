Double Spend Proofs
===================

dsproofs.txt
------------

This data was collected on 2021-05-21 on a BCHN v23.0.0 node logging the
output of the RPC command `getdsprooflist 0 true`,

The data file contains counts of DSP's kept in memory by the node at
particular points in time (POSIX timestamps).

A high rate of DSP's was observed initially. This rate seemed out of
the ordinary, so I decided to log and plot it.


20210521-dsproofs_2.png
-----------------------

This is a plot of some of the above data. It was exported from a live
plot run with

```
$ gnuplot -p -e "set xtics rotate; set xdata time; set timefmt \"%s\"; plot 'dsproofs.txt' using 1:2 with lines; while (1) { pause 3; replot; }"
```
